import { Container, Heading, SimpleGrid } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { WorkGridItem } from '../components/grid-item'

import thumbnaclopop from '../public/images/works/naclopop.png'
import thumbcreative4umusic from '../public/images/works/creative4umusic.png'
import thumbSouknet from '../public/images/works/souksnet.jpg'
import thumbAllomychef from '../public/images/works/allomychef.png'


const Works = () => (
  <Layout title="Works">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Works
      </Heading>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section>
          <WorkGridItem id="naclopop" title="Naclopop" thumbnail={thumbnaclopop}>
          Naclopop.org is a third-web decentralized crowdfunding platform built on Solana blockchain. 
          Discover a new way to fund creative projects, connect with like-minded individuals, 
          and explore a decentralized ecosystem of contributors and creators
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem
            id="music"
            title="Full Stack React Web"
            thumbnail={thumbSouknet}
          >
            This Full Stack React Web App with Shazam Music API Integration is an innovative and powerful tool for anyone looking to discover new music, explore artists, and delve into the world of music. With the rise of music streaming services, 
            the ability to search for and discover new music has become more important than ever before.
             
          </WorkGridItem>
        </Section>

        <Section delay={0.1}>
          <WorkGridItem
            id="souksnet"
            title="Souksnet"
            thumbnail={thumbcreative4umusic}
          >
           Find the best listings for products, services, and events on Souksnet. 
           Compare prices, read reviews, and make informed decisions before making a purchase or attending an event. Start exploring today!
           The project involves building a web platform that allows users to create and manage listings for products, services, and events. Users can sign up for an account, create their own listings, and search for listings created by others. 
          </WorkGridItem>
        </Section>
        <Section delay={0.1}>
          <WorkGridItem id="allomychef" thumbnail={thumbAllomychef} title="Allomychef">
          Allomychef is a website that connects individuals who offer cooking services
          The platform allows users to share their culinary skills and experiences with others, and also provides an opportunity 
          for home cooks to monetize their cooking expertise by selling meals to customers. 
          Through Allomychef, users can discover new and unique dishes, meet new people, and explore different cultural cuisines.
          </WorkGridItem>
        </Section>
      </SimpleGrid>

    </Container>
  </Layout>
)

export default Works
export { getServerSideProps } from '../components/chakra'
