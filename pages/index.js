import NextLink from 'next/link'
import {
  Link,
  Container,
  Heading,
  Box,
  Button,
  List,
  ListItem,
  useColorModeValue,
  chakra
} from '@chakra-ui/react'
import Paragraph from '../components/paragraph'
import { BioSection, BioYear } from '../components/bio'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { IoLogoTwitter, IoLogoInstagram, IoLogoGithub } from 'react-icons/io5'
import Image from 'next/image'

const ProfileImage = chakra(Image, {
  shouldForwardProp: prop => ['width', 'height', 'src', 'alt'].includes(prop)
})

const Home = () => (
  <Layout>
    <Container>
      <Box
        borderRadius="lg"
        mb={6}
        p={3}
        textAlign="center"
        bg={useColorModeValue('whiteAlpha.500', 'whiteAlpha.200')}
        css={{ backdropFilter: 'blur(10px)' }}
      >
        Hello, I&apos;m a indie Full Stack Developer based in France!
      </Box>

      <Box display={{ md: 'flex' }}>
        <Box flexGrow={1}>
          <Heading as="h2" variant="page-title">
            Nabil Laâziri
          </Heading>
          <p>Digital Craftsman ( Full Stack Developer )</p>
        </Box>
        <Box
          flexShrink={0}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
          textAlign="center"
        >
          <Box
            borderColor="whiteAlpha.800"
            borderWidth={2}
            borderStyle="solid"
            w="100px"
            h="100px"
            display="inline-block"
            borderRadius="full"
            overflow="hidden"
          >
            <ProfileImage
              src="/images/nabil.jpg"
              alt="Profile image"
              borderRadius="full"
              width="100"
              height="100"
            />
          </Box>
        </Box>
      </Box>

      <Section delay={0.1}>
        <Heading as="h3" variant="section-title">
          Work
        </Heading>
        <Paragraph>
          I am a freelance full-stack developer based in Nantes with a passion for creating digital products and services that make a positive impact. 
          I thrive on the entire product development process, from brainstorming and designing to coding and launching.
          As dedicated coder, I spend time every day honing my skills and staying up-to-date with the latest industry trends. 
          This daily practice has helped me become a proficient developer with a keen eye for detail and a commitment to delivering high-quality products.
          In my free time, I indulge my passion for astronomy, exploring the wonders of the universe. 
          I also enjoy expressing myself creatively through writing and staying active and energized by running.
          Currently, I am running my own agency, 
          {' '} <Link as={NextLink} href="https://creative-4u.com/"
          passHref scroll={false} target="_blank"
          rel="noopener noreferrer"
          >Creative4u</Link> where I provide top-notch services to my clients. With my broad skill set and passion for technology, 
          I strive to create innovative solutions that exceed their expectations.
          
          </Paragraph>
      </Section>
      <Section delay={0.2}>
        <Heading as="h3" variant="section-title">
          Bio
        </Heading>
        <BioSection>
          <BioYear>1984</BioYear>
          Born in Rouen (Normandie), France.
        </BioSection>
        <BioSection>
          <BioYear>2006</BioYear>
          Completed my degree in Spanish Language at the Universitat de Barcelona, located in Barcelona, Spain.
        </BioSection>
        <BioSection>
          <BioYear>2010</BioYear>
          Completed the Degree in a Graduade Scholarship in Computer Science at the University of Corte, France.
        </BioSection>
        <BioSection>
          <BioYear>2011</BioYear>
          Worked at Byseacycle
          As the owner, business developer, and webmaster of Byseacycle, a cycle hire service in Weymouth during the 2012 Olympics, 
          I provided visitors with easy and eco-friendly transportation options to explore the town and surrounding area. 
          Byseacycle was selected alongside one other company to claim the market of the Olympic Games in Weymouth, 
          despite facing competition from 12 other cycle hire companies. Through my roles as business developer and webmaster, 
          I implemented marketing strategies and built and maintained the company website, 
          which allowed customers to easily access rental information and make reservations online.Weymouth, UK.
        </BioSection>
        <BioSection>
          <BioYear>2012 to present</BioYear>
          Working as a freelancer since 2012, I have extensive experience in developing custom software solutions, creating interactive web applications, 
          and deploying smart contracts on various blockchain platforms. 
          My expertise includes Python, Django, JavaScript, Node.js, Vue.js, Nuxt.js, Jest, Next.js.
          I am constantly learning and expanding my skillset to provide cutting-edge solutions to my clients.
          As part of my portfolio, I have built a website using Chakra UI and Three.js with 3D animation, showcasing my skills in front-end development and graphics programming.
          I work closely with my clients to understand their unique requirements and deliver tailored solutions that meet their specific needs.
        </BioSection>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          I ♥
        </Heading>
        <Paragraph>
          Art, Writing, Music, Astronomy,
          Running, Mathematics, Deep Learning, 
          Machine Learning, Blochain Technology.
        </Paragraph>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          On the web
        </Heading>
        <List>
          <ListItem>
            <Link href="https://gitlab.com/n.laaziri" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoGithub />}
              >
                @nabster76
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link href="https://twitter.com/NabilLaaziri" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoTwitter />}
              >
                @NabilLaaziri
              </Button>
            </Link>
          </ListItem>
          <ListItem>
            <Link href="https://www.instagram.com/nabil__laaziri" target="_blank">
              <Button
                variant="ghost"
                colorScheme="teal"
                leftIcon={<IoLogoInstagram />}
              >
                @nabil__laaziri
              </Button>
            </Link>
          </ListItem>
        </List>
      </Section>
     </Container>
  </Layout>
)

export default Home
export { getServerSideProps } from '../components/chakra'
