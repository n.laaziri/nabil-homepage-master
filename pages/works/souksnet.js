import {
  Container,
  Badge,
  Link,
  List,
  ListItem,
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="souksnet">
    <Container>
      <Title>
       Souksnet <Badge>2022-</Badge>
      </Title>
      <P>
      Souksnet - Discover and Compare the Best Listings Online
Find the best listings for products, services, and events on Souksnet. Compare prices, read reviews, and make informed decisions before making a purchase or attending an event. Start exploring today!
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://www.souksnet.com/">
          https://www.souksnet.com/ <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>WebApp</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>Laravel, Angular</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/souksnet_01.png" alt="souksnet" />
      <WorkImage src="/images/works/souksnet_02.png" alt="souksnet" />
      <WorkImage src="/images/works/souksnet_03.png" alt="souksnet" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
