import {
  Container,
  Badge,
  Link,
  List,
  ListItem,
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="creative4u-music">
    <Container>
      <Title>
      creative4u-music <Badge>2022-</Badge>
      </Title>
      <P>
      Our Full Stack React Web App with Shazam Music API Integration is a cutting-edge project that showcases the power of modern web development technologies. 
      With the rise of music streaming services, the ability to discover new music and explore the world of music has become more important than ever before. 
      Our app seeks to provide users with a seamless, intuitive, and visually appealing experience that enables them to explore music, artists, and lyrics all in one place.
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://creative4u-music.netlify.app/">
          https://creative4u-music.netlify.app/ <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>WebApp</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>React, NodeJS</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/creative4u-music_01.png" alt="creative4u-music" />
      <WorkImage src="/images/works/creative4u-music_02.png" alt="creative4u-music" />
      <WorkImage src="/images/works/creative4u-music_03.png" alt="creative4u-music" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
