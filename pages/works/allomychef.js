import {
  Container,
  Badge,
  Link,
  List,
  ListItem,
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="allomychef">
    <Container>
      <Title>
        Allomychef <Badge>2022-</Badge>
      </Title>
      <P>
      Allomychef.fr is a decentralized crowdfunding platform built on the Solana blockchain.
       Our platform is designed to enable creators to raise funds for their creative projects from a global audience of supporters. 
      With its fast, secure, and low-cost transactions, allomychef.org is redefining the world of crowdfunding on the blockchain..
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://allomychef.fr/">
          https://allomychef.fr/ <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>WebApp</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>Python, Django</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/allomychef_03.png" alt="allomychef" />
      <WorkImage src="/images/works/allomychef_01.png" alt="allomychef" />
      <WorkImage src="/images/works/allomychef_02.png" alt="allomychef" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
