import {
  Container,
  Badge,
  Link,
  List,
  ListItem,
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="Naclopop">
    <Container>
      <Title>
        Naclopop <Badge>2022-</Badge>
      </Title>
      <P>
      Naclopop.org is a third-web decentralized crowdfunding platform built on Solana blockchain. 
      Discover a new way to fund creative projects, connect with like-minded individuals, 
      and explore a decentralized ecosystem of contributors and creators. With fast, secure, and low-cost transactions, 
      Naclopop.org is redefining the world of crowdfunding on the blockchain. 
      Try it out today and start supporting the next generation of innovators!
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://naclopop.netlify.app/">
          https://naclopop.netlify.app/ <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>WebApp</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>NodeJS, Vite, Third-web, Solana Blockchain</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/naclopop_01.png" alt="Naclopop" />
      <WorkImage src="/images/works/naclopop_02.png" alt="Naclopop" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
